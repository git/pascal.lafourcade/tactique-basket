<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Inscription</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/floating-labels/">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>
  <link href="floating-labels.css" rel="stylesheet">
</head>

<body>
  <?php if(!isset($_POST['submit'])){
    ?>
    <form class="form-signin" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
      <div class="text-center mb-4">
        <img class="mb-4" src="ballon.png" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Inscription</h1>
      </div>

      <div class="form-label-group">
        <input type="text" class="form-control" placeholder="Username" id="validationPseudo" required autofocus name="pseudo">
        <label for="validationPseudo">Pseudo</label>
      </div>
      <div class="form-label-group">
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name="mail">
        <label for="inputEmail">Email</label>
      </div>

      <div class="form-label-group">
        <input type="password" id="inputPassword" id="change" class="form-control" placeholder="Password" required name="pwd">
        <label for="inputPassword">Mot de passe</label>
      </div>
      <div class="form-label-group">
        <input type="password" id="inputPassword" id="change" class="form-control" placeholder="Password" required name="cpwd">
        <label for="inputPassword">Confirmer mot de passe</label>
      </div>

      <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">S'inscrire</button>
    </form>

    <?php
  }else{
    try{
      if($_POST['pwd'] == $_POST['cpwd']){
        $db = new PDO('sqlite:BDD.db');

        $id =current($db->query("SELECT COUNT(*) FROM utilisateur")->fetch());
        $a = "1";
        $idG= intval($id + $a);

        $sql =("INSERT INTO utilisateur (pseudo,mail,motDePasse,idGalerie,mdpOublie) VALUES (:pseudo, :mail, :pwd ,:idG,0)");
        $stmt =$db->prepare($sql);

        $mail = filter_input(INPUT_POST,'mail');
        $stmt->bindValue(':mail',$mail,PDO::PARAM_STR);

        $pwd = filter_input(INPUT_POST,'pwd');
        $stmt->bindValue(':pwd',$pwd,PDO::PARAM_STR);

        $pseudo = filter_input(INPUT_POST,'pseudo');
        $stmt->bindValue(':pseudo',$pseudo,PDO::PARAM_STR);

        $stmt->bindValue(':idG',$idG,PDO::PARAM_STR);

        $success = $stmt->execute();
        $maile = $_POST['mail'];
        $sql =$db->query('INSERT INTO GALERIE (mail,idGalerie) VALUES ("'.$maile.'","'.$idG.'")');

        header('Location: basket.php?idgalerie='.$idG.'');
        exit();
      }
      else{
        echo "mauvais mot de passe";
      }
      $db=null;
    } catch(PDOException $e)
    {
    }
  }


  ?>
</body>
</html>
