<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Connexion</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/floating-labels/">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>
  <link href="floating-labels.css" rel="stylesheet">
</head>

<body>
  <?php
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;
  require 'PHPMailer/src/Exception.php';
  require 'PHPMailer/src/PHPMailer.php';
  require 'PHPMailer/src/SMTP.php';
  if(!isset($_POST['submit'])){
    ?>
    <form class="form-signin" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
      <div class="text-center mb-4">
        <h2>Mot de passe oublié</h2>
        <img class="mb-4" src="ballon.png" alt="" width="72" height="72">
        <h3 class="h3 mb-3 font-weight-normal">Création du nouveau mot de passe</h3>
      </div>
      <div class="form-label-group">
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name="mail">
        <label for="inputEmail">Email de réception du mot de passe</label>
      </div>

      <p>Merci de renseigner l'adresse mail à laquelle envoyer votre nouveau mot de passe. Une fois reçu, vous pourrez alors vous connecter et changer votre mot de passe.</p>

      <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Envoyer le nouveau mot de passe</button>
      <a href="Inscription.php">Inscription</a>
    </form>


    <?php
  }else{
    try{

      $db = new PDO('sqlite:BDD.db');
      $mail=$_POST['mail'];
      $existe=$db->query('SELECT COUNT(*) FROM UTILISATEUR WHERE mail="'.$mail.'"');
      $row = $existe->fetchAll(PDO::FETCH_COLUMN, 0);
      $res=intval($row[0]);
      var_dump($mail);
      var_dump($res);

      function genererChaineAleatoire($longueur = 14)
      {
       $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
       $longueurMax = strlen($caracteres);
       $chaineAleatoire = '';
       for ($i = 0; $i < $longueur; $i++)
       {
         $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
       }
       return $chaineAleatoire;
     }

     if($res==1){
       function smtpMailer($code) {
         $email=$_POST['mail'];
         $mail = new PHPMailer(TRUE);

         try {
    //Server settings
    $mail->SMTPDebug = 0;//Enable verbose debug output
    $mail->isSMTP();//Send using SMTP
    $mail->Host = 'smtp.gmail.com';//Set the SMTP server to send through
    $mail->SMTPAuth = true;//Enable SMTP authentication
    $mail->Username = 'basketprojet4@gmail.com';//SMTP username
    $mail->Password = 'jesuismdp4';//SMTP password
    $mail->SMTPSecure = 'tls';//Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port = 587;//TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('no-reply@basket.com', 'Basket Team');
    $mail->addAddress($email);    

    //Content
    $mail->isHTML(true); //Set email format to HTML
    $mail->Subject = 'Nouveau mot de passe';
    $mail->Body    = 'Voici votre nouveau mot de passe, celui ci sera temporaire jusqu\'à votre prochaine connexion <b>'.$code.'</b>';

    $mail->send();
    echo 'Message has been sent';
  } catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
  }
}


$code = genererChaineAleatoire();
$result = smtpmailer($code);
if (true !== $result)
{
  $statement = $db->query('UPDATE UTILISATEUR SET motDePasse = "'.$code.'", mdpOublie=1 WHERE mail = "'.$mail.'"');
  $row = $statement->fetchAll(PDO::FETCH_COLUMN, 0);
  $res=intval($row[0]);
  header('Location: vueConnexion.php');
  exit();
}
}else{
  header('Location: renvoiMotDePasse.php');
  exit();
}

$db=null;
} catch(PDOException $e)
{
}
}


?>
</body>
</html>
