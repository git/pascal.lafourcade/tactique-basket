<!DOCTYPE html>
<html lang="fr">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="basket.css">
	<title>Basket</title>
</head>
<!-- <body onresize="load()"> -->
	<body>
		<?php
		$monid=$_GET['idgalerie'];
		?>
		<!-- Navbar -->
		<nav class="navbar navbar-expand-lg navbar-light" id="barre_nav">
			<div id="titre">
				<img id="image_ballon" src="ballon.png" width="30" height="30" alt="">
				<a class="navbar-brand text-white"  href="#">BASKET</a>
			</div>
			<button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link text-white" href="galerie.php?galerie=<?=$monid?>">Galerie</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Nouvelle page
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item " href="#" onclick="faireterrain()" >FULL TERRAIN</a>

							<div class="dropdown-divider"></div>
							<a class="dropdown-item" id="demo" href="#" onclick="faireDemiTerrainHaut()">PANIER HAUT</a>
							<a class="dropdown-item" id="demo" href="#" onclick="faireDemiTerrainBas()">PANIER BAS</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Partager
						</a>
						<div class="dropdown-menu text-white" aria-labelledby="navbarDropdown">
							<a href="#" class="button dropdown-item" id="btn-download">PNG</a>
							<div class="dropdown-divider"></div>
							<script src="https://cdn.jsdelivr.net/npm/three@0.118.3/build/three.js"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
							<button class="dropdown-item" id="downloadpdf">PDF</button>
							<div class="dropdown-divider"></div>
							<button class="dropdown-item" id="download" onclick="exporterTex()">TeX</button>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link text-white" href="#">Sauvegarder</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Modifier
						</a>
						<div class="dropdown-menu text-white" aria-labelledby="navbarDropdown">
							<button class="button dropdown-item" onclick="togglePopup()">Attaquant</button>
							<div class="dropdown-divider"></div>
							<button class="button dropdown-item" onclick="togglePopup2()">Defenseur</button>
							<div class="dropdown-divider"></div>
							<button class="button dropdown-item" onclick="togglePopup3()">Terrain</button>
							<div class="dropdown-divider"></div>
							<button class="button dropdown-item" onclick="togglePopup4()">Raquette</button>
							<div class="dropdown-divider"></div>
							<button class="button dropdown-item" onclick="togglePopup5()">Zone</button>
							<div class="dropdown-divider"></div>
							<button class="button dropdown-item" onclick="togglePopup6()">Mouvement</button>
							<div class="dropdown-divider"></div>
							<button class="button dropdown-item" onclick="togglePopup7()">Touche</button>
							<div class="dropdown-divider"></div>
							<button class="button dropdown-item" onclick="togglePopup8()">Ligne</button>

						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  onclick="togglePopupTaille()">
							TailleJoueur
						</a>
					</li>


					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="togglePopupligne()">
							TailleLigne
						</a>
					</li>
				</ul>
			</div>
		</nav>


		<div class="container-fluid">
			<div class="row">
				<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block sidebar collapse">
					<div class="row list-group list-group">
						<div >
							<div class="list-group list-group-horizontal" id="bouton" role="tablist">
								<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">CREATION
								</a>
								<a class="list-group-item list-group-item-action" data-toggle="list" id="list-tab" href="#list-profile" role="tab" aria-controls="profile">VUE ARBRE
								</a>
							</div>
						</div>
						<div>
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
									<div class="sidebar-sticky pt-3">
										<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted font-weight-bold">
											<span>AJOUTER JOUEUR</span>
											<a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
												<span data-feather="plus-circle"></span>
											</a>
										</h6>
										<ul class="nav flex-column" id="ul">
											<li class="nav-item">
												<a class="nav-link" onclick="ajouterjoueur()" id="bouttonattaquant">
													<span data-feather="file"></span>
													ATTAQUANT
												</a>
											</li>
											<li class="nav-item" id="bouttonadd">
												<a class="nav-link "  onclick="ajouteradversaire()" id="bouttonadversaire">
													<span data-feather="shopping-cart"></span>
													DÉFENSEUR
												</a>
											</ul>

											<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted font-weight-bold">
												<span>ACTIONS</span>
												<a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
													<span data-feather="plus-circle"></span>
												</a>
											</h6>
											<ul class="nav flex-column mb-2" id="ul">
												<li class="nav-item">
													<a class="nav-link" onclick="triggerdonnerlaballe()" id="bouttondonner">
														<span data-feather="file-text"></span>
														DONNER LA BALLE
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" onclick="triggerfairepasse()" id="bouttonpasse">
														<span data-feather="file-text"></span>
														PASSE
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link"  onclick="deplace()" id="bouttondeplacer">
														<span data-feather="file-text"></span>
														DÉPLACER
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" onclick="dribble()" id="bouttondribbler">
														<span data-feather="file-text"></span>
														DRIBBLER
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" id="bouttonecran" onclick="ecraner()">
														<span data-feather="file-text"></span>
														ÉCRAN
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" onclick="tire()" id="bouttontirer">
														<span data-feather="file-text"></span>
														TIRER
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" onclick="influer()" id="bouttoninfluence">
														<span data-feather="file-text"></span>
														ZONE D'INFLUENCE
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" onclick="retourarriere()" id="bouttonretour">
														<span data-feather="file-text"></span>
														RETOUR ARRIERE 
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" onclick="editer()" id="bouttonediter">
														<span data-feather="file-text"></span>
														EDITER
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" onclick="effacer()" id="bouttoneffacer">
														<span data-feather="file-text"></span>
														EFFACER
													</a>
												</li>
												<li class="nav-item yay" id="gauche">
													<a class="nav-link" onclick="tournergauche()" id="tournergauche">
														<span data-feather="file"></span>
														<--
													</a>
												</li>
												<li class="nav-item yay" id="droite">
													<a class="nav-link" onclick="tournerdroite()" id="tournerdroite">
														<span data-feather="file"></span>
													-->
												</a>
											</li>
											<li class="nav-item yay" id="perso">
												<form name="myform" action="" method="get">
													Entrez un angle personalisé	 <br>
													<input type="text" name="inputbox" value="" id="myTextBox">
													<input type="button" name="button" value="Set" onclick="showData(this.form)">
												</form>
												
											</li>
										</ul>
									</div>
								</div>
								<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
									<div class="sidebar-sticky pt-3">
										<ul class="nav flex-column" id="ul">
											<li class="nav-item">
												<a class="nav-link active" href="#">
													<span data-feather="home"></span>
													<span class="sr-only">(current)</span>
												</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="#">
													<span data-feather="file"></span>
													<img id="arbre" src="arbre.PNG" width="30" height="30" alt="">
												</a>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>				
				</nav>


				<div id="divTerrains">
					<canvas id="terrain" style="display:block;"></canvas>
					<script src ="Terrains/Horizontal/entier.js"></script>

					<canvas id="demiTerrainDroit" style="display:none;"></canvas>
					<script src ="Terrains/Horizontal/demiDroit.js"></script>

					<canvas id="demiTerrainGauche" style="display:none;"></canvas>
					<script src ="Terrains/Horizontal/demiGauche.js"></script>
				</div>
				<div class="popup" id="popup-1">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup()">&times;</div>
						<div id="barre"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="carre"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input type="text" id="resultat" class="skin_resultat" size=20 name="affichage_couleur" disabled="true" value="#FF0000" />

						</form>


					</div>
				</div>
				<div class="popup" id="popup-2">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup2()">&times;</div>
						<div id="labarre"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1def" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="lecarre"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2def" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input type="text" id="resultatdef" class="skin_resultat" size=20 name="affichage_couleur" disabled="true" value="#4A51CE " />

						</form>
					</div>
				</div>

				<div class="popup" id="popup-3">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup3()">&times;</div>
						<div id="labarreTer"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1ter" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="lecarret"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2ter" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input type="text" id="resultatter" class="skin_resultat" size=20 name="affichage_couleur" disabled="true" value="#808080 " />

						</form>
					</div>
				</div>
				<div class="popup" id="popup-4">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup4()">&times;</div>
						<div id="labarreinTer"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1inter" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="lecarreint"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2inter" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input type="text" id="resultatinter" class="skin_resultat" size=20 name="affichage_couleur"  disabled="true"value="#9E0E40" />

						</form>
					</div>
				</div>

				<div class="popup" id="popup-5">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup5()">&times;</div>
						<div id="labarrezone"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1zone" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="lecarrezone"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2zone" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input  id="resultatzone" class="skin_resultat" size=20 name="affichage_couleur" disabled="true" value="#40E0D0" />

						</form>
					</div>
				</div>


				<div class="popup" id="popup-6">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup6()">&times;</div>
						<div id="labarremouv"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1mouv" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="lecarremouv"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2mouv" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input  id="resultatmouv" class="skin_resultat" size=20 name="affichage_couleur" disabled="true" value="#000000" />

						</form>
					</div>
				</div>

				<div class="popup" id="popup-7">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup7()">&times;</div>
						<div id="labarretouche"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1touche" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="lecarretouche"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2touche" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input type="text" id="resultatdelatouche" class="skin_resultat" size=20 name="affichage_couleur"  disabled="true"value="#9E0E40" />

						</form>
					</div>
				</div>

				<div class="popup" id="popup-8">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopup8()">&times;</div>
						<div id="labarreligne"    class="skin_barre"    onmousedown="clique('barre')"> </div>
						<div id="curseur1ligne" class="skin_curseur1" onmousedown="clique('barre')"> </div>
						<div id="lecarreligne"    class="skin_carre"    onmousedown="clique('carre')"> </div>
						<div id="curseur2ligne" class="skin_curseur2" onmousedown="clique('carre')"> </div>

						<form>
							<input type="text" id="resultatdelaligne" class="skin_resultat" size=20 name="affichage_couleur"  disabled="true"value="#000000" />

						</form>
					</div>
				</div>


				<div class="popupT" id="popup-taille">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopupTaille()">&times;</div>
						<button id="boutonatt+" class="test">+</button>
						<button id="boutonatt-" class="test">-</button>
					</div>

				</div>
				<div class="popupL" id="popup-ligne">
					<div class="overlay"></div>
					<div class="content">
						<div class="close-btn" onclick="togglePopupligne()">&times;</div>
						<button id="boutonL+" class="test" >+</button>
						<button id="boutonL-" class="test">-</button>
					</div>

				</div>
				<script>
					function afficherFullTerrain() {
						document.getElementById("terrain").style.display = "block";
						document.getElementById("demiTerrainDroit").style.display = "none";
						document.getElementById("demiTerrainGauche").style.display = "none";
					}

					function afficherDemiTerrainDroit() {
						document.getElementById("terrain").style.display = "none";
						document.getElementById("demiTerrainDroit").style.display = "block";
						document.getElementById("demiTerrainGauche").style.display = "none";
					}

					function afficherDemiTerrainGauche() {
						document.getElementById("terrain").style.display = "none";
						document.getElementById("demiTerrainDroit").style.display = "none";
						document.getElementById("demiTerrainGauche").style.display = "block";
					}
				</script>
				
				<button type="button" class="btn btn-warning fixed-bottom font-weight-bold" onclick="nextStep()">+</button>
				<form method="post" id="inputSaves">
					<input type="hidden" id="tailleattaq" name="tailleattaq">
					<input type="hidden" id="numero0" name="numero0">
					<input type="hidden" id="posX0" name="posX0">
					<input type="hidden" id="posY0" name="posY0">
					<input type="hidden" id="rayon0" name="rayon0">
					<input type="hidden" id="couleur0" name="couleur0">
					<input type="hidden" id="gottheball0" name="gottheball0">
					<input type="hidden" id="setball0" name="setball0">
					<input type="hidden" id="sedeplace0" name="sedeplace0">
					<input type="hidden" id="tiring0" name="tiring0">
					<input type="hidden" id="isDragging0" name="isDragging0">
					<input type="hidden" id="dribbb0" name="dribbb0">

					<input type="hidden" id="numero1" name="numero1">
					<input type="hidden" id="posX1" name="posX1">
					<input type="hidden" id="posY1" name="posY1">
					<input type="hidden" id="rayon1" name="rayon1">
					<input type="hidden" id="couleur1" name="couleur1">
					<input type="hidden" id="gottheball1" name="gottheball1">
					<input type="hidden" id="setball1" name="setball1">
					<input type="hidden" id="sedeplace1" name="sedeplace1">
					<input type="hidden" id="tiring1" name="tiring1">
					<input type="hidden" id="isDragging1" name="isDragging1">
					<input type="hidden" id="dribbb1" name="dribbb1">

					<input type="hidden" id="numero2" name="numero2">
					<input type="hidden" id="posX2" name="posX2">
					<input type="hidden" id="posY2" name="posY2">
					<input type="hidden" id="rayon2" name="rayon2">
					<input type="hidden" id="couleur2" name="couleur2">
					<input type="hidden" id="gottheball2" name="gottheball2">
					<input type="hidden" id="setball2" name="setball2">
					<input type="hidden" id="sedeplace2" name="sedeplace2">
					<input type="hidden" id="tiring2" name="tiring2">
					<input type="hidden" id="isDragging2" name="isDragging2">
					<input type="hidden" id="dribbb2" name="dribbb2">

					<input type="hidden" id="numero3" name="numero3">
					<input type="hidden" id="posX3" name="posX3">
					<input type="hidden" id="posY3" name="posY3">
					<input type="hidden" id="rayon3" name="rayon3">
					<input type="hidden" id="couleur3" name="couleur3">
					<input type="hidden" id="gottheball3" name="gottheball3">
					<input type="hidden" id="setball3" name="setball3">
					<input type="hidden" id="sedeplace3" name="sedeplace3">
					<input type="hidden" id="tiring3" name="tiring3">
					<input type="hidden" id="isDragging3" name="isDragging3">
					<input type="hidden" id="dribbb3" name="dribbb3">

					<input type="hidden" id="numero4" name="numero4">
					<input type="hidden" id="posX4" name="posX4">
					<input type="hidden" id="posY4" name="posY4">
					<input type="hidden" id="rayon4" name="rayon4">
					<input type="hidden" id="couleur4" name="couleur4">
					<input type="hidden" id="gottheball4" name="gottheball4">
					<input type="hidden" id="setball4" name="setball4">
					<input type="hidden" id="sedeplace4" name="sedeplace4">
					<input type="hidden" id="tiring4" name="tiring4">
					<input type="hidden" id="isDragging4" name="isDragging4">
					<input type="hidden" id="dribbb4" name="dribbb4">

					<input type="hidden" id="numero5" name="numero5">
					<input type="hidden" id="posX5" name="posX5">
					<input type="hidden" id="posY5" name="posY5">
					<input type="hidden" id="rayon5" name="rayon5">
					<input type="hidden" id="couleur5" name="couleur5">
					<input type="hidden" id="gottheball5" name="gottheball5">
					<input type="hidden" id="setball5" name="setball5">
					<input type="hidden" id="sedeplace5" name="sedeplace5">
					<input type="hidden" id="tiring5" name="tiring5">
					<input type="hidden" id="isDragging5" name="isDragging5">
					<input type="hidden" id="dribbb5" name="dribbb5">
					<input type="hidden" id="tailledef" name="tailledef">




					<input type="hidden" id="ddefX0" name="ddefX0">
					<input type="hidden" id="ddefY0" name="ddefY0">
					<input type="hidden" id="ddefrayon0" name="ddefrayon0">
					<input type="hidden" id="ddefcouleur0" name="ddefcouleur0">
					<input type="hidden" id="ddefisDragging0" name="ddefisDragging0">
					<input type="hidden" id="ddefsetball0" name="ddefsetball0">
					<input type="hidden"id="ddefhig0" name="ddefhig0">
					<input type="hidden"  id="ddefangle0" name="ddefangle0">
					<input type="hidden" id="ddefinverse0" name="ddefinverse0">
					<input type="hidden" id="ddefretour0" name="ddefretour0">
					<input type="hidden" id="ddefrangle0" name="ddefrangle0">

					<input type="hidden" id="ddefX1" name="ddefX1">
					<input type="hidden" id="ddefY1" name="ddefY1">
					<input type="hidden" id="ddefrayon1" name="ddefrayon1">
					<input type="hidden" id="ddefcouleur1" name="ddefcouleur1">
					<input type="hidden" id="ddefisDragging1" name="ddefisDragging1">
					<input type="hidden" id="ddefsetball1" name="ddefsetball1">
					<input type="hidden" id="ddefhig1" name="ddefhig1">
					<input type="hidden" id="ddefangle1" name="ddefangle1">
					<input type="hidden" id="ddefinverse1" name="ddefinverse1">
					<input type="hidden" id="ddefretour1" name="ddefretour1">
					<input type="hidden" id="ddefrangle1" name="ddefrangle1">

					<input type="hidden" id="ddefX2" name="ddefX2">
					<input type="hidden" id="ddefY2" name="ddefY2">
					<input type="hidden" id="ddefrayon2" name="ddefrayon2">
					<input type="hidden" id="ddefcouleur2" name="ddefcouleur2">
					<input type="hidden" id="ddefisDragging2" name="ddefisDragging2">
					<input type="hidden" id="ddefsetball2" name="ddefsetball2">
					<input type="hidden" id="ddefhig2" name="ddefhig2">
					<input type="hidden" id="ddefangle2" name="ddefangle2">
					<input type="hidden" id="ddefinverse2" name="ddefinverse2">
					<input type="hidden" id="ddefretour2" name="ddefretour2">
					<input type="hidden" id="ddefrangle2" name="ddefrangle2">

					<input type="hidden" id="ddefX3" name="ddefX3">
					<input type="hidden" id="ddefY3" name="ddefY3">
					<input type="hidden" id="ddefrayon3" name="ddefrayon3">
					<input type="hidden" id="ddefcouleur3" name="ddefcouleur3">
					<input type="hidden" id="ddefisDragging3" name="ddefisDragging3">
					<input type="hidden" id="ddefsetball3" name="ddefsetball3">
					<input type="hidden" id="ddefhig3" name="ddefhig3">
					<input type="hidden" id="ddefangle3" name="ddefangle3">
					<input type="hidden" id="ddefinverse3" name="ddefinverse3">
					<input type="hidden" id="ddefretour3" name="ddefretour3">
					<input type="hidden" id="ddefrangle3" name="ddefrangle3">

					<input type="hidden" id="ddefX4" name="ddefX4">
					<input type="hidden" id="ddefY4" name="ddefY4">
					<input type="hidden" id="ddefrayon4" name="ddefrayon4">
					<input type="hidden" id="ddefcouleur4" name="ddefcouleur4">
					<input type="hidden" id="ddefisDragging4" name="ddefisDragging4">
					<input type="hidden" id="ddefsetball4" name="ddefsetball4">
					<input type="hidden" id="ddefhig4" name="ddefhig4">
					<input type="hidden" id="ddefangle4" name="ddefangle4">
					<input type="hidden" id="ddefinverse4" name="ddefinverse4">
					<input type="hidden" id="ddefretour4" name="ddefretour4">
					<input type="hidden" id="ddefrangle4" name="ddefrangle4">

					<input type="hidden" id="ddefX5" name="ddefX5">
					<input type="hidden" id="ddefY5" name="ddefY5">
					<input type="hidden" id="ddefrayon5" name="ddefrayon5">
					<input type="hidden" id="ddefcouleur5" name="ddefcouleur5">
					<input type="hidden" id="ddefisDragging5" name="ddefisDragging5">
					<input type="hidden" id="ddefsetball5" name="ddefsetball5">
					<input type="hidden" id="ddefhig5" name="ddefhig5">
					<input type="hidden" id="ddefangle5" name="ddefangle5">
					<input type="hidden" id="ddefinverse5" name="ddefinverse5">
					<input type="hidden" id="ddefretour5" name="ddefretour5">
					<input type="hidden" id="ddefrangle5" name="ddefrangle5">





					<input type="hidden" id="tailleddep" name="tailleddep">
					<input type="hidden" id="ddepX0" name="ddepX0">
					<input type="hidden" id="ddepY0" name="ddepY0">
					<input type="hidden" id="ddepedit0" name="ddepedit0">
					<input type="hidden" id="ddeprayon0" name="ddeprayon0">
					<input type="hidden" id="ddepdbase0" name="ddepdbase0">
					<input type="hidden" id="ddepec0" name="ddepec0">
					<input type="hidden" id="ddephig0" name="ddephig0">
					<input type="hidden" id="ddepangle0" name="ddepangle0">
					<input type="hidden" id="ddepinverse0" name="ddepinverse0">
					<input type="hidden" id="ddepnumero0" name="ddepnumero0">

					<input type="hidden" id="ddepX1" name="ddepX1">
					<input type="hidden" id="ddepY1" name="ddepY1">
					<input type="hidden" id="ddepedit1" name="ddepedit1">
					<input type="hidden" id="ddeprayon1" name="ddeprayon1">
					<input type="hidden" id="ddepdbase1" name="ddepdbase1">
					<input type="hidden" id="ddepec1" name="ddepec1">
					<input type="hidden" id="ddephig1" name="ddephig1">
					<input type="hidden" id="ddepangle1" name="ddepangle1">
					<input type="hidden" id="ddepinverse1" name="ddepinverse1">
					<input type="hidden" id="ddepnumero1" name="ddepnumero1">

					<input type="hidden" id="ddepX2" name="ddepX2">
					<input type="hidden" id="ddepY2" name="ddepY2">
					<input type="hidden" id="ddepedit2" name="ddepedit2">
					<input type="hidden" id="ddeprayon2" name="ddeprayon2">
					<input type="hidden" id="ddepdbase2" name="ddepdbase2">
					<input type="hidden" id="ddepec2" name="ddepec2">
					<input type="hidden" id="ddephig2" name="ddephig2">
					<input type="hidden" id="ddepangle2" name="ddepangle2">
					<input type="hidden" id="ddepinverse2" name="ddepinverse2">
					<input type="hidden" id="ddepnumero2" name="ddepnumero2">

					<input type="hidden" id="ddepX3" name="ddepX3">
					<input type="hidden" id="ddepY3" name="ddepY3">
					<input type="hidden" id="ddepedit3" name="ddepedit3">
					<input type="hidden" id="ddeprayon3" name="ddeprayon3">
					<input type="hidden" id="ddepdbase3" name="ddepdbase3">
					<input type="hidden" id="ddepec3" name="ddepec3">
					<input type="hidden" id="ddephig3" name="ddephig3">
					<input type="hidden" id="ddepangle3" name="ddepangle3">
					<input type="hidden" id="ddepinverse3" name="ddepinverse3">
					<input type="hidden" id="ddepnumero3" name="ddepnumero3">





					<input type="hidden" id="tailleddep2" name="tailleddep2">
					<input type="hidden" id="ddep2X0" name="ddep2X0">
					<input type="hidden" id="ddep2Y0" name="ddep2Y0">
					<input type="hidden" id="ddep2edit0" name="ddep2edit0">
					<input type="hidden" id="ddep2rayon0" name="ddep2rayon0">
					<input type="hidden" id="ddep2dbase0" name="ddep2dbase0">
					<input type="hidden" id="ddep2ec0" name="ddep2ec0">
					<input type="hidden" id="ddep2hig0" name="ddep2hig0">
					<input type="hidden" id="ddep2angle0" name="ddep2angle0">
					<input type="hidden" id="ddep2inverse0" name="ddep2inverse0">
					<input type="hidden" id="ddep2numero0" name="ddep2numero0">

					<input type="hidden" id="ddep2X1" name="ddep2X1">
					<input type="hidden" id="ddep2Y1" name="ddep2Y1">
					<input type="hidden" id="ddep2edit1" name="ddep2edit1">
					<input type="hidden" id="ddep2rayon1" name="ddep2rayon1">
					<input type="hidden" id="ddep2dbase1" name="ddep2dbase1">
					<input type="hidden" id="ddep2ec1" name="ddep2ec1">
					<input type="hidden" id="ddep2hig1" name="ddep2hig1">
					<input type="hidden" id="ddep2angle1" name="ddep2angle1">
					<input type="hidden" id="ddep2inverse1" name="ddep2inverse1">
					<input type="hidden" id="ddep2numero1" name="ddep2numero1">




					<input type="hidden" id="tailleddri" name="tailleddri">
					<input type="hidden" id="ddriX0" name="ddriX0">
					<input type="hidden" id="ddriY0" name="ddriY0">
					<input type="hidden" id="ddriedit0" name="ddriedit0">
					<input type="hidden" id="ddrirayon0" name="ddrirayon0">
					<input type="hidden" id="ddridbase0" name="ddridbase0">


					<input type="hidden" id="ddriX1" name="ddriX1">
					<input type="hidden" id="ddriY1" name="ddriY1">
					<input type="hidden" id="ddriedit1" name="ddriedit1">
					<input type="hidden" id="ddrirayon1" name="ddrirayon1">
					<input type="hidden" id="ddridbase1" name="ddridbase1">


					<input type="hidden" id="ddriX2" name="ddriX2">
					<input type="hidden" id="ddriY2" name="ddriY2">
					<input type="hidden" id="ddriedit2" name="ddriedit2">
					<input type="hidden" id="ddrirayon2" name="ddrirayon2">
					<input type="hidden" id="ddridbase2" name="ddridbase2">


					<input type="hidden" id="ddriX3" name="ddriX3">
					<input type="hidden" id="ddriY3" name="ddriY3">
					<input type="hidden" id="ddriedit3" name="ddriedit3">
					<input type="hidden" id="ddrirayon3" name="ddrirayon3">
					<input type="hidden" id="ddridbase3" name="ddridbase3">


					<input type="hidden" id="ddriX4" name="ddriX4">
					<input type="hidden" id="ddriY4" name="ddriY4">
					<input type="hidden" id="ddriedit4" name="ddriedit4">
					<input type="hidden" id="ddrirayon4" name="ddrirayon4">
					<input type="hidden" id="ddridbase4" name="ddridbase4">






					<input type="hidden" id="tailleddri2" name="tailleddri2">
					<input type="hidden" id="ddri2X0" name="ddri2X0">
					<input type="hidden" id="ddri2Y0" name="ddri2Y0">
					<input type="hidden" id="ddri2edit0" name="ddri2edit0">
					<input type="hidden" id="ddri2rayon0" name="ddri2rayon0">
					<input type="hidden" id="ddri2dbase0" name="ddri2dbase0">

					<input type="hidden" id="ddri2X1" name="ddri2X1">
					<input type="hidden" id="ddri2Y1" name="ddri2Y1">
					<input type="hidden" id="ddri2edit1" name="ddri2edit1">
					<input type="hidden" id="ddri2rayon1" name="ddri2rayon1">
					<input type="hidden" id="ddri2dbase1" name="ddri2dbase1">

					<input type="hidden" id="ddri2X2" name="ddri2X2">
					<input type="hidden" id="ddri2Y2" name="ddri2Y2">
					<input type="hidden" id="ddri2edit2" name="ddri2edit2">
					<input type="hidden" id="ddri2rayon2" name="ddri2rayon2">
					<input type="hidden" id="ddri2dbase2" name="ddri2dbase2">

					<input type="hidden" id="ddri2X3" name="ddri2X3">
					<input type="hidden" id="ddri2Y3" name="ddri2Y3">
					<input type="hidden" id="ddri2edit3" name="ddri2edit3">
					<input type="hidden" id="ddri2rayon3" name="ddri2rayon3">
					<input type="hidden" id="ddri2dbase3" name="ddri2dbase3">

					<input type="hidden" id="ddri2X4" name="ddri2X4">
					<input type="hidden" id="ddri2Y4" name="ddri2Y4">
					<input type="hidden" id="ddri2edit4" name="ddri2edit4">
					<input type="hidden" id="ddri2rayon4" name="ddri2rayon4">
					<input type="hidden" id="ddri2dbase4" name="ddri2dbase4">








					<input type="hidden" id="tailleppasse" name="tailleppasse">
					<input type="hidden" id="ppassedep0" name="ppassedep0">
					<input type="hidden" id="ppassearr0" name="ppassearr0">
					<input type="hidden" id="ppassenum0" name="ppassenum0">

					<input type="hidden" id="ppassedep1" name="ppassedep1">
					<input type="hidden" id="ppassearr1" name="ppassearr1">
					<input type="hidden" id="ppassenum1" name="ppassenum1">



					<input type="hidden" id="taillelaste" name="taillelaste">
					<input type="hidden" id="lasteact0" name="lasteact0">
					<input type="hidden" id="lastearr0" name="lastearr0">
					<input type="hidden" id="lastedep0" name="lastedep0">
					<input type="hidden" id="lastenum0" name="lastenum0">

					<input type="hidden" id="lasteact1" name="lasteact1">
					<input type="hidden" id="lastearr1" name="lastearr1">
					<input type="hidden" id="lastedep1" name="lastedep1">
					<input type="hidden" id="lastenum1" name="lastenum1">


					<input type="hidden" id="taillezon" name="taillezon">
					<input type="hidden" id="zonX0" name="zonX0">
					<input type="hidden" id="zonY0" name="zonY0">
					<input type="hidden" id="zonrayon0" name="zonrayon0">
					<input type="hidden" id="zonisDragging0" name="zonisDragging0">
					<input type="hidden" id="zonedit0" name="zonedit0">
					<input type="hidden" id="zonnum0" name="zonnum0">

					<input type="hidden" id="zonX1" name="zonX1">
					<input type="hidden" id="zonY1" name="zonY1">
					<input type="hidden" id="zonrayon1" name="zonrayon1">
					<input type="hidden" id="zonisDragging1" name="zonisDragging1">
					<input type="hidden" id="zonedit1" name="zonedit1">
					<input type="hidden" id="zonnum1" name="zonnum1">

					<input type="hidden" id="zonX2" name="zonX2">
					<input type="hidden" id="zonY2" name="zonY2">
					<input type="hidden" id="zonrayon2" name="zonrayon2">
					<input type="hidden" id="zonisDragging2" name="zonisDragging2">
					<input type="hidden" id="zonedit2" name="zonedit2">
					<input type="hidden" id="zonnum2" name="zonnum2">

					<input type="hidden" id="zonX3" name="zonX3">
					<input type="hidden" id="zonY3" name="zonY3">
					<input type="hidden" id="zonrayon3" name="zonrayon3">
					<input type="hidden" id="zonisDragging3" name="zonisDragging3">
					<input type="hidden" id="zonedit3" name="zonedit3">
					<input type="hidden" id="zonnum3" name="zonnum3">

					<input type="hidden" id="zonX4" name="zonX4">
					<input type="hidden" id="zonY4" name="zonY4">
					<input type="hidden" id="zonrayon4" name="zonrayon4">
					<input type="hidden" id="zonisDragging4" name="zonisDragging4">
					<input type="hidden" id="zonedit4" name="zonedit4">
					<input type="hidden" id="zonnum4" name="zonnum4">

					<input type="hidden" id="typeterrain" name="typeterrain">

					<?php
					$db = new PDO('sqlite:BDD.db');
					$nomPlan2 = $_GET['nomPlan'];
					$viensDeGalerie = $_GET['viensDeGalerie'];
					?>
					<input type="hidden" id="dejaexistant" name="dejaexistant" value= <?php echo $viensDeGalerie ?>>
					<input type="hidden" id="nomPlan2" name="nomPlan2" value= <?php echo $nomPlan2 ?>>



					<input type="hidden" id="nomPlan" name="nomPlan" >
					<input type="submit" class="btn btn-warning fixed-bottom font-weight-bold ml-5" name="inputSave" id="inputSave" value="SAVE" onclick="updateBtn()">
				</input>
			</form>
			<?php 
			if(isset($_POST['inputSave'])) {
				$db = new PDO('sqlite:BDD.db');


				$numGalerie = $_GET['idgalerie'];
				$idPlanCharge = $_GET['idPlanCharge'];
				$viensDeGalerie = $_GET['viensDeGalerie'];
				if($viensDeGalerie == null)
				{
					$viensDeGalerie=0;
				}



				if($viensDeGalerie == 0){
					$nom=$_POST['nomPlan'];
					if($nom == null){
						$nomcreer = $db->query('SELECT MAX(idPlan) FROM PLAN');
						$rows2 = $nomcreer->fetchAll(PDO::FETCH_COLUMN, 0);
						$nome=intval($rows2[0]) +1;
						$statement = $db->query('INSERT INTO PLAN (nomPlan,appartient) 
							VALUES ("'.$nome.'","'.$numGalerie.'");');
						$db->query('DELETE FROM PLAN WHERE nomPlan="'.$nome.'"');
					}else{
						$statement = $db->query('INSERT INTO PLAN (nomPlan,appartient) 
							VALUES ("'.$nom.'","'.$numGalerie.'");');
					}





					$appart = $db->query('SELECT MAX(idPlan) FROM PLAN');
					$rows2 = $appart->fetchAll(PDO::FETCH_COLUMN, 0);
					$appar=intval($rows2[0]);

					$taille= $_POST['tailleattaq'];
					$tailleattaq=intval($taille);


					for ($i = 0; $i <= $tailleattaq-1; $i++) {
						$numero=$_POST['numero'.$i];
						$posX=$_POST['posX'.$i];
						$posY=$_POST['posY'.$i];
						$rayon=$_POST['rayon'.$i];
						$couleur=$_POST['couleur'.$i];
						$gottheball=$_POST['gottheball'.$i];
						$setball=$_POST['setball'.$i];
						$sedeplace=$_POST['sedeplace'.$i];
						$tiring=$_POST['tiring'.$i];
						$isDragging=$_POST['isDragging'.$i];
						$dribb=$_POST['dribbb'.$i];

						$statement = $db->query('INSERT INTO JOUEUR (numero,posX,posY,rayon,couleur,gottheball,setball,sedeplace,tiring,isDragging,appartient,dribbb) 
							VALUES ("'.$numero.'","'.$posX.'","'.$posY.'","'.$rayon.'","'.$couleur.'","'.$gottheball.'","'.$setball.'","'.$sedeplace.'","'.$tiring.'","'.$isDragging.'","'.$appar.'","'.$dribb.'");');
					}
					$tailled= $_POST['tailledef'];
					$tailledef=intval($tailled);
					for ($j = 0; $j <= $tailledef-1; $j++) {
						$numerodef=$j +1;
						$defX=$_POST['ddefX'.$j];
						$defY=$_POST['ddefY'.$j];
						$defrayon=$_POST['ddefrayon'.$j];
						$defcouleur=$_POST['ddefcouleur'.$j];
						$defisDragging=$_POST['ddefisDragging'.$j];
						$defsetball=$_POST['ddefsetball'.$j];
						$defhig=$_POST['ddefhig'.$j];
						$defangle=$_POST['ddefangle'.$j];
						$definverse=$_POST['ddefinverse'.$j];
						$defrretour=$_POST['ddefretour'.$j];
						$defrangle=$_POST['ddefrangle'.$j];

						$state = $db->query('INSERT INTO ADVERSAIRE (X,Y,rayon,couleur,isDragging,setball,hig,angle,inverse,retour,rangle,appartient,numero) 
							VALUES ("'.$defX.'","'.$defY.'","'.$defrayon.'","'.$defcouleur.'","'.$defisDragging.'","'.$defsetball.'","'.$defhig.'","'.$defangle.'","'.$definverse.'","'.$defrretour.'","'.$defrangle.'","'.$appar.'","'.$numerodef.'");');
					}

					$tailleddep= $_POST['tailleddep'];
					$tailledep=intval($tailleddep);
					for ($j = 0; $j <= $tailledep-1; $j++) {
						$numerodep=$j +1;
						$depX=$_POST['ddepX'.$j];
						$depY=$_POST['ddepY'.$j];
						$ddepedit=$_POST['ddepedit'.$j];
						$ddeprayon=$_POST['ddeprayon'.$j];
						$ddepdbase=$_POST['ddepdbase'.$j];
						$ddepec=$_POST['ddepec'.$j];
						$dephig=$_POST['ddephig'.$j];
						$depangle=$_POST['ddepangle'.$j];
						$depinverse=$_POST['ddepinverse'.$j];

						$state = $db->query('INSERT INTO DEPLACEMENT (X,Y,edit,rayon,dbase,ec,hig,angle,inverse,appartient,numero) 
							VALUES ("'.$depX.'","'.$depY.'","'.$ddepedit.'","'.$ddeprayon.'","'.$ddepdbase.'","'.$ddepec.'","'.$dephig.'","'.$depangle.'","'.$depinverse.'","'.$appar.'","'.$numerodep.'");');
					}

					$tailleddep2= $_POST['tailleddep2'];
					$tailledep2=intval($tailleddep2);
					for ($j = 0; $j <= $tailledep2-1; $j++) {
						$numerodep2=$j +1;
						$depX2=$_POST['ddep2X'.$j];
						$depY2=$_POST['ddep2Y'.$j];
						$ddepedit2=$_POST['ddep2edit'.$j];
						$ddeprayon2=$_POST['ddep2rayon'.$j];
						$ddepdbase2=$_POST['ddep2dbase'.$j];
						$ddepec2=$_POST['ddep2ec'.$j];
						$dephig2=$_POST['ddep2hig'.$j];
						$depangle2=$_POST['ddep2angle'.$j];
						$depinverse2=$_POST['ddep2inverse'.$j];

						$state = $db->query('INSERT INTO DEPLACEMENT2PTS (X,Y,edit,rayon,dbase,ec,hig,angle,inverse,appartient,numero) 
							VALUES ("'.$depX2.'","'.$depY2.'","'.$ddepedit2.'","'.$ddeprayon2.'","'.$ddepdbase2.'","'.$ddepec2.'","'.$dephig2.'","'.$depangle2.'","'.$depinverse2.'","'.$appar.'","'.$numerodep2.'");');
					}

					$tailleddri= $_POST['tailleddri'];
					$tailleddrii=intval($tailleddri);
					for ($j = 0; $j <= $tailleddrii-1; $j++) {
						$numerodri=$j +1;
						$ddriX=$_POST['ddriX'.$j];
						$ddriY=$_POST['ddriY'.$j];
						$ddriedit=$_POST['ddriedit'.$j];
						$ddrirayon=$_POST['ddrirayon'.$j];
						$ddridbase=$_POST['ddridbase'.$j];

						$state = $db->query('INSERT INTO DRIBBLE (X,Y,edit,rayon,dbase,appartient,numero) 
							VALUES ("'.$ddriX.'","'.$ddriY.'","'.$ddriedit.'","'.$ddrirayon.'","'.$ddridbase.'","'.$appar.'","'.$numerodri.'");');
					}

					$tailleddri2= $_POST['tailleddri2'];
					$tailleddrii2=intval($tailleddri2);
					for ($j = 0; $j <= $tailleddrii2-1; $j++) {
						$numerodri2=$j +1;
						$ddri2X=$_POST['ddri2X'.$j];
						$ddri2Y=$_POST['ddri2Y'.$j];
						$ddri2edit=$_POST['ddri2edit'.$j];
						$ddri2rayon=$_POST['ddri2rayon'.$j];
						$ddri2dbase=$_POST['ddri2dbase'.$j];

						$state = $db->query('INSERT INTO DRIBLE2PTS (X,Y,edit,rayon,dbase,appartient,numero) 
							VALUES ("'.$ddri2X.'","'.$ddri2Y.'","'.$ddri2edit.'","'.$ddri2rayon.'","'.$ddri2dbase.'","'.$appar.'","'.$numerodri2.'");');
					}

					$tailleppasse= $_POST['tailleppasse'];
					$tailleppass=intval($tailleppasse);
					for ($j = 0; $j <= $tailleppass-1; $j++) {
						$ppassenum=$j +1;
						$ppassedep=$_POST['ppassedep'.$j];
						$ppassearr=$_POST['ppassearr'.$j];

						$state = $db->query('INSERT INTO PASSES (numdep,numarr,appartient,numero) 
							VALUES ("'.$ppassedep.'","'.$ppassearr.'","'.$appar.'","'.$ppassenum.'");');
					}


					$taillezon= $_POST['taillezon'];
					$taillezone=intval($taillezon);
					for ($j = 0; $j <= $taillezone-1; $j++) {
						$zonenum=$j +1;
						$zonX=$_POST['zonX'.$j];
						$zonY=$_POST['zonY'.$j];
						$zonrayon=$_POST['zonrayon'.$j];
						$zonisDragging=$_POST['zonisDragging'.$j];
						$zonedit=$_POST['zonedit'.$j];

						$state = $db->query('INSERT INTO ZONE (X,Y,rayon,isDragging,edit,appartient,numero) 
							VALUES ("'.$zonX.'","'.$zonY.'","'.$zonrayon.'","'.$zonisDragging.'","'.$zonedit.'","'.$appar.'","'.$zonenum.'");');
					}

					$taillelaste= $_POST['taillelaste'];
					$taillelast=intval($taillelaste);
					for ($j = 0; $j <= $taillelast-1; $j++) {
						$lastnum=$j +1;
						$lasteact=$_POST['lasteact'.$j];
						$lastearr=$_POST['lastearr'.$j];
						$lastedep=$_POST['lastedep'.$j];

						$state = $db->query('INSERT INTO LAST (action,arrive,depart,appartient,numero) 
							VALUES ("'.$lasteact.'","'.$lastearr.'","'.$lastedep.'","'.$appar.'","'.$lastnum.'");');
					}

					$typeter=$_POST['typeterrain'];
					$state = $db->query('INSERT INTO TYPETERRAIN (type,appartient) 
						VALUES ("'.$typeter.'","'.$appar.'");');




				}
				
				if( $viensDeGalerie ==1 && i==null){


					$nom=$_POST['nomPlan'];
					$numGalerie = $_GET['idgalerie'];
					$idPlanCharge = $_GET['idPlanCharge'];



					$db->query('DELETE FROM ADVERSAIRE WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM DEPLACEMENT WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM DEPLACEMENT2PTS WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM DRIBBLE WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM DRIBLE2PTS WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM JOUEUR WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM LAST WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM PASSES WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM ZONE WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM TYPETERRAIN WHERE appartient="'.$idPlanCharge.'"');
					$db->query('DELETE FROM PLAN WHERE idPlan="'.$idPlanCharge.'"');
					



					$statement = $db->query('INSERT INTO PLAN (nomPlan,idPlan,appartient) 
						VALUES ("'.$nom.'","'.$idPlanCharge.'","'.$numGalerie.'");');
					$taille= $_POST['tailleattaq'];
					$tailleattaq=intval($taille);



					for ($i = 0; $i <= $tailleattaq-1; $i++) {
						$numero=$_POST['numero'.$i];
						$posX=$_POST['posX'.$i];
						$posY=$_POST['posY'.$i];
						$rayon=$_POST['rayon'.$i];
						$couleur=$_POST['couleur'.$i];
						$gottheball=$_POST['gottheball'.$i];
						$setball=$_POST['setball'.$i];
						$sedeplace=$_POST['sedeplace'.$i];
						$tiring=$_POST['tiring'.$i];
						$isDragging=$_POST['isDragging'.$i];
						$dribb=$_POST['dribbb'.$i];

						$statement = $db->query('INSERT INTO JOUEUR (numero,posX,posY,rayon,couleur,gottheball,setball,sedeplace,tiring,isDragging,appartient,dribbb) 
							VALUES ("'.$numero.'","'.$posX.'","'.$posY.'","'.$rayon.'","'.$couleur.'","'.$gottheball.'","'.$setball.'","'.$sedeplace.'","'.$tiring.'","'.$isDragging.'","'.$idPlanCharge.'","'.$dribb.'");');
					}
					$tailled= $_POST['tailledef'];
					$tailledef=intval($tailled);
					for ($j = 0; $j <= $tailledef-1; $j++) {
						$numerodef=$j +1;
						$defX=$_POST['ddefX'.$j];
						$defY=$_POST['ddefY'.$j];
						$defrayon=$_POST['ddefrayon'.$j];
						$defcouleur=$_POST['ddefcouleur'.$j];
						$defisDragging=$_POST['ddefisDragging'.$j];
						$defsetball=$_POST['ddefsetball'.$j];
						$defhig=$_POST['ddefhig'.$j];
						$defangle=$_POST['ddefangle'.$j];
						$definverse=$_POST['ddefinverse'.$j];
						$defrretour=$_POST['ddefretour'.$j];
						$defrangle=$_POST['ddefrangle'.$j];

						$state = $db->query('INSERT INTO ADVERSAIRE (X,Y,rayon,couleur,isDragging,setball,hig,angle,inverse,retour,rangle,appartient,numero) 
							VALUES ("'.$defX.'","'.$defY.'","'.$defrayon.'","'.$defcouleur.'","'.$defisDragging.'","'.$defsetball.'","'.$defhig.'","'.$defangle.'","'.$definverse.'","'.$defrretour.'","'.$defrangle.'","'.$idPlanCharge.'","'.$numerodef.'");');
					}

					$tailleddep= $_POST['tailleddep'];
					$tailledep=intval($tailleddep);
					for ($j = 0; $j <= $tailledep-1; $j++) {
						$numerodep=$j +1;
						$depX=$_POST['ddepX'.$j];
						$depY=$_POST['ddepY'.$j];
						$ddepedit=$_POST['ddepedit'.$j];
						$ddeprayon=$_POST['ddeprayon'.$j];
						$ddepdbase=$_POST['ddepdbase'.$j];
						$ddepec=$_POST['ddepec'.$j];
						$dephig=$_POST['ddephig'.$j];
						$depangle=$_POST['ddepangle'.$j];
						$depinverse=$_POST['ddepinverse'.$j];

						$state = $db->query('INSERT INTO DEPLACEMENT (X,Y,edit,rayon,dbase,ec,hig,angle,inverse,appartient,numero) 
							VALUES ("'.$depX.'","'.$depY.'","'.$ddepedit.'","'.$ddeprayon.'","'.$ddepdbase.'","'.$ddepec.'","'.$dephig.'","'.$depangle.'","'.$depinverse.'","'.$idPlanCharge.'","'.$numerodep.'");');
					}

					$tailleddep2= $_POST['tailleddep2'];
					$tailledep2=intval($tailleddep2);
					for ($j = 0; $j <= $tailledep2-1; $j++) {
						$numerodep2=$j +1;
						$depX2=$_POST['ddep2X'.$j];
						$depY2=$_POST['ddep2Y'.$j];
						$ddepedit2=$_POST['ddep2edit'.$j];
						$ddeprayon2=$_POST['ddep2rayon'.$j];
						$ddepdbase2=$_POST['ddep2dbase'.$j];
						$ddepec2=$_POST['ddep2ec'.$j];
						$dephig2=$_POST['ddep2hig'.$j];
						$depangle2=$_POST['ddep2angle'.$j];
						$depinverse2=$_POST['ddep2inverse'.$j];

						$state = $db->query('INSERT INTO DEPLACEMENT2PTS (X,Y,edit,rayon,dbase,ec,hig,angle,inverse,appartient,numero) 
							VALUES ("'.$depX2.'","'.$depY2.'","'.$ddepedit2.'","'.$ddeprayon2.'","'.$ddepdbase2.'","'.$ddepec2.'","'.$dephig2.'","'.$depangle2.'","'.$depinverse2.'","'.$idPlanCharge.'","'.$numerodep2.'");');
					}

					$tailleddri= $_POST['tailleddri'];
					$tailleddrii=intval($tailleddri);
					for ($j = 0; $j <= $tailleddrii-1; $j++) {
						$numerodri=$j +1;
						$ddriX=$_POST['ddriX'.$j];
						$ddriY=$_POST['ddriY'.$j];
						$ddriedit=$_POST['ddriedit'.$j];
						$ddrirayon=$_POST['ddrirayon'.$j];
						$ddridbase=$_POST['ddridbase'.$j];

						$state = $db->query('INSERT INTO DRIBBLE (X,Y,edit,rayon,dbase,appartient,numero) 
							VALUES ("'.$ddriX.'","'.$ddriY.'","'.$ddriedit.'","'.$ddrirayon.'","'.$ddridbase.'","'.$idPlanCharge.'","'.$numerodri.'");');
					}

					$tailleddri2= $_POST['tailleddri2'];
					$tailleddrii2=intval($tailleddri2);
					for ($j = 0; $j <= $tailleddrii2-1; $j++) {
						$numerodri2=$j +1;
						$ddri2X=$_POST['ddri2X'.$j];
						$ddri2Y=$_POST['ddri2Y'.$j];
						$ddri2edit=$_POST['ddri2edit'.$j];
						$ddri2rayon=$_POST['ddri2rayon'.$j];
						$ddri2dbase=$_POST['ddri2dbase'.$j];

						$state = $db->query('INSERT INTO DRIBLE2PTS (X,Y,edit,rayon,dbase,appartient,numero) 
							VALUES ("'.$ddri2X.'","'.$ddri2Y.'","'.$ddri2edit.'","'.$ddri2rayon.'","'.$ddri2dbase.'","'.$idPlanCharge.'","'.$numerodri2.'");');
					}

					$tailleppasse= $_POST['tailleppasse'];
					$tailleppass=intval($tailleppasse);
					for ($j = 0; $j <= $tailleppass-1; $j++) {
						$ppassenum=$j +1;
						$ppassedep=$_POST['ppassedep'.$j];
						$ppassearr=$_POST['ppassearr'.$j];

						$state = $db->query('INSERT INTO PASSES (numdep,numarr,appartient,numero) 
							VALUES ("'.$ppassedep.'","'.$ppassearr.'","'.$idPlanCharge.'","'.$ppassenum.'");');
					}


					$taillezon= $_POST['taillezon'];
					$taillezone=intval($taillezon);
					for ($j = 0; $j <= $taillezone-1; $j++) {
						$zonenum=$j +1;
						$zonX=$_POST['zonX'.$j];
						$zonY=$_POST['zonY'.$j];
						$zonrayon=$_POST['zonrayon'.$j];
						$zonisDragging=$_POST['zonisDragging'.$j];
						$zonedit=$_POST['zonedit'.$j];

						$state = $db->query('INSERT INTO ZONE (X,Y,rayon,isDragging,edit,appartient,numero) 
							VALUES ("'.$zonX.'","'.$zonY.'","'.$zonrayon.'","'.$zonisDragging.'","'.$zonedit.'","'.$idPlanCharge.'","'.$zonenum.'");');
					}

					$taillelaste= $_POST['taillelaste'];
					$taillelast=intval($taillelaste);
					for ($j = 0; $j <= $taillelast-1; $j++) {
						$lastnum=$j +1;
						$lasteact=$_POST['lasteact'.$j];
						$lastearr=$_POST['lastearr'.$j];
						$lastedep=$_POST['lastedep'.$j];

						$state = $db->query('INSERT INTO LAST (action,arrive,depart,appartient,numero) 
							VALUES ("'.$lasteact.'","'.$lastearr.'","'.$lastedep.'","'.$idPlanCharge.'","'.$lastnum.'");');
					}
					$typeter=$_POST['typeterrain'];
					$state = $db->query('INSERT INTO TYPETERRAIN (type,appartient) 
						VALUES ("'.$typeter.'","'.$idPlanCharge.'");');
					$i==1;
				}


			} else {

			}
			?>
			<form >
				<?php
				$db = new PDO('sqlite:BDD.db');

				$appartient = $_GET['idPlanCharge'];

				$nbr =current($db->query('SELECT COUNT(*) FROM JOUEUR WHERE appartient = "'.$appartient.'"')->fetch());
				$taille=intval($nbr);


				$numero0=current($db->query('SELECT numero
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$posX0=current($db->query('SELECT posX
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$posY0=current($db->query('SELECT posY
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$rayon0=current($db->query('SELECT rayon
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$couleur0=current($db->query('SELECT couleur
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$gottheball0=current($db->query('SELECT gottheball
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$setball0=current($db->query('SELECT setball
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$sedeplace0=current($db->query('SELECT sedeplace
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$tiring0=current($db->query('SELECT tiring
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$isDragging0=current($db->query('SELECT isDragging
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dribb0=current($db->query('SELECT dribbb
					FROM JOUEUR
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$numero1=current($db->query('SELECT numero
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$posX1=current($db->query('SELECT posX
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$posY1=current($db->query('SELECT posY
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$rayon1=current($db->query('SELECT rayon
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$couleur1=current($db->query('SELECT couleur
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$gottheball1=current($db->query('SELECT gottheball
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$setball1=current($db->query('SELECT setball
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$sedeplace1=current($db->query('SELECT sedeplace
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$tiring1=current($db->query('SELECT tiring
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$isDragging1=current($db->query('SELECT isDragging
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dribb1=current($db->query('SELECT dribbb
					FROM JOUEUR
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());


				$numero2=current($db->query('SELECT numero
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$posX2=current($db->query('SELECT posX
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$posY2=current($db->query('SELECT posY
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$rayon2=current($db->query('SELECT rayon
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$couleur2=current($db->query('SELECT couleur
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$gottheball2=current($db->query('SELECT gottheball
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$setball2=current($db->query('SELECT setball
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$sedeplace2=current($db->query('SELECT sedeplace
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$tiring2=current($db->query('SELECT tiring
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$isDragging2=current($db->query('SELECT isDragging
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$dribb2=current($db->query('SELECT dribbb
					FROM JOUEUR
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());



				$numero3=current($db->query('SELECT numero
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$posX3=current($db->query('SELECT posX
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$posY3=current($db->query('SELECT posY
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$rayon3=current($db->query('SELECT rayon
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$couleur3=current($db->query('SELECT couleur
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$gottheball3=current($db->query('SELECT gottheball
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$setball3=current($db->query('SELECT setball
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$sedeplace3=current($db->query('SELECT sedeplace
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$tiring3=current($db->query('SELECT tiring
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$isDragging3=current($db->query('SELECT isDragging
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$dribb3=current($db->query('SELECT dribbb
					FROM JOUEUR
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());



				$numero4=current($db->query('SELECT numero
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$posX4=current($db->query('SELECT posX
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$posY4=current($db->query('SELECT posY
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$rayon4=current($db->query('SELECT rayon
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$couleur4=current($db->query('SELECT couleur
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$gottheball4=current($db->query('SELECT gottheball
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$setball4=current($db->query('SELECT setball
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$sedeplace4=current($db->query('SELECT sedeplace
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$tiring4=current($db->query('SELECT tiring
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$isDragging4=current($db->query('SELECT isDragging
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$dribb4=current($db->query('SELECT dribbb
					FROM JOUEUR
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());






				$nb =current($db->query('SELECT COUNT(*) FROM ADVERSAIRE WHERE appartient = "'.$appartient.'"')->fetch());
				$tailleAd=intval($nb);

				$defX0=current($db->query('SELECT X
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defY0=current($db->query('SELECT Y
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defrayon0=current($db->query('SELECT rayon
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defcouleur0=current($db->query('SELECT couleur
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defisDragging0=current($db->query('SELECT isDragging
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defsetball0=current($db->query('SELECT setball
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defhig0=current($db->query('SELECT hig
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defangle0=current($db->query('SELECT angle
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$definverse0=current($db->query('SELECT inverse
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defrretour0=current($db->query('SELECT retour
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$defrangle0=current($db->query('SELECT rangle
					FROM ADVERSAIRE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());


				$defX1=current($db->query('SELECT X
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defY1=current($db->query('SELECT Y
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defrayon1=current($db->query('SELECT rayon
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defcouleur1=current($db->query('SELECT couleur
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defisDragging1=current($db->query('SELECT isDragging
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defsetball1=current($db->query('SELECT setball
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defhig1=current($db->query('SELECT hig
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defangle1=current($db->query('SELECT angle
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$definverse1=current($db->query('SELECT inverse
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defrretour1=current($db->query('SELECT retour
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$defrangle1=current($db->query('SELECT rangle
					FROM ADVERSAIRE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());



				$defX2=current($db->query('SELECT X
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defY2=current($db->query('SELECT Y
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defrayon2=current($db->query('SELECT rayon
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defcouleur2=current($db->query('SELECT couleur
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defisDragging2=current($db->query('SELECT isDragging
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defsetball2=current($db->query('SELECT setball
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defhig2=current($db->query('SELECT hig
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defangle2=current($db->query('SELECT angle
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$definverse2=current($db->query('SELECT inverse
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defrretour2=current($db->query('SELECT retour
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$defrangle2=current($db->query('SELECT rangle
					FROM ADVERSAIRE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());



				$defX3=current($db->query('SELECT X
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defY3=current($db->query('SELECT Y
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defrayon3=current($db->query('SELECT rayon
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defcouleur3=current($db->query('SELECT couleur
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defisDragging3=current($db->query('SELECT isDragging
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defsetball3=current($db->query('SELECT setball
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defhig3=current($db->query('SELECT hig
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defangle3=current($db->query('SELECT angle
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$definverse3=current($db->query('SELECT inverse
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defrretour3=current($db->query('SELECT retour
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$defrangle3=current($db->query('SELECT rangle
					FROM ADVERSAIRE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());


				$defX4=current($db->query('SELECT X
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defY4=current($db->query('SELECT Y
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defrayon4=current($db->query('SELECT rayon
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defcouleur4=current($db->query('SELECT couleur
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defisDragging4=current($db->query('SELECT isDragging
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defsetball4=current($db->query('SELECT setball
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defhig4=current($db->query('SELECT hig
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defangle4=current($db->query('SELECT angle
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$definverse4=current($db->query('SELECT inverse
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defrretour4=current($db->query('SELECT retour
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$defrangle4=current($db->query('SELECT rangle
					FROM ADVERSAIRE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());






				$nbrdep =current($db->query('SELECT COUNT(*) FROM DEPLACEMENT WHERE appartient = "'.$appartient.'"')->fetch());
				$tailledep=intval($nbrdep);
				$depX0=current($db->query('SELECT X
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$depY0=current($db->query('SELECT Y
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$depedit0=current($db->query('SELECT edit
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$deprayon0=current($db->query('SELECT rayon
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$depdbase0=current($db->query('SELECT dbase
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$depec0=current($db->query('SELECT ec
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dephig0=current($db->query('SELECT hig
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$depangle0=current($db->query('SELECT angle
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$depinverse0=current($db->query('SELECT inverse
					FROM DEPLACEMENT
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$depX1=current($db->query('SELECT X
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$depY1=current($db->query('SELECT Y
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$depedit1=current($db->query('SELECT edit
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$deprayon1=current($db->query('SELECT rayon
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$depdbase1=current($db->query('SELECT dbase
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$depec1=current($db->query('SELECT ec
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dephig1=current($db->query('SELECT hig
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$depangle1=current($db->query('SELECT angle
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$depinverse1=current($db->query('SELECT inverse
					FROM DEPLACEMENT
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());

				$depX2=current($db->query('SELECT X
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$depY2=current($db->query('SELECT Y
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$depedit2=current($db->query('SELECT edit
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$deprayon2=current($db->query('SELECT rayon
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$depdbase2=current($db->query('SELECT dbase
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$depec2=current($db->query('SELECT ec
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$dephig2=current($db->query('SELECT hig
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$depangle2=current($db->query('SELECT angle
					FROM DEPLACEMENT
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$depinverse2=current($db->query('SELECT inverse
					FROM DEPLACEMENT
					WHERE numero =3 and appartient = "'.$appartient.'"')->fetch());

				$depX3=current($db->query('SELECT X
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$depY3=current($db->query('SELECT Y
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$depedit3=current($db->query('SELECT edit
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$deprayon3=current($db->query('SELECT rayon
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$depdbase3=current($db->query('SELECT dbase
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$depec3=current($db->query('SELECT ec
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$dephig3=current($db->query('SELECT hig
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$depangle3=current($db->query('SELECT angle
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$depinverse3=current($db->query('SELECT inverse
					FROM DEPLACEMENT
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());





				$nbrdep2 =current($db->query('SELECT COUNT(*) FROM DEPLACEMENT2PTS WHERE  appartient = "'.$appartient.'"')->fetch());
				$tailledep2=intval($nbrdep2);
				$dep2X0=current($db->query('SELECT X
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2Y0=current($db->query('SELECT Y
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2edit0=current($db->query('SELECT edit
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2rayon0=current($db->query('SELECT rayon
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2dbase0=current($db->query('SELECT dbase
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2ec0=current($db->query('SELECT ec
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2hig0=current($db->query('SELECT hig
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2angle0=current($db->query('SELECT angle
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dep2inverse0=current($db->query('SELECT inverse
					FROM DEPLACEMENT2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$dep2X1=current($db->query('SELECT X
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2Y1=current($db->query('SELECT Y
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2edit1=current($db->query('SELECT edit
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2rayon1=current($db->query('SELECT rayon
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2dbase1=current($db->query('SELECT dbase
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2ec1=current($db->query('SELECT ec
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2hig1=current($db->query('SELECT hig
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2angle1=current($db->query('SELECT angle
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dep2inverse1=current($db->query('SELECT inverse
					FROM DEPLACEMENT2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());





				$nbrdri =current($db->query('SELECT COUNT(*) FROM DRIBBLE WHERE appartient = "'.$appartient.'"')->fetch());
				$tailledri=intval($nbrdri);
				$driX0=current($db->query('SELECT X
					FROM DRIBBLE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$driY0=current($db->query('SELECT Y
					FROM DRIBBLE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$driedit0=current($db->query('SELECT edit
					FROM DRIBBLE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$drirayon0=current($db->query('SELECT rayon
					FROM DRIBBLE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dridbase0=current($db->query('SELECT dbase
					FROM DRIBBLE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$driX1=current($db->query('SELECT X
					FROM DRIBBLE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$driY1=current($db->query('SELECT Y
					FROM DRIBBLE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$driedit1=current($db->query('SELECT edit
					FROM DRIBBLE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$drirayon1=current($db->query('SELECT rayon
					FROM DRIBBLE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dridbase1=current($db->query('SELECT dbase
					FROM DRIBBLE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());

				$driX2=current($db->query('SELECT X
					FROM DRIBBLE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$driY2=current($db->query('SELECT Y
					FROM DRIBBLE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$driedit2=current($db->query('SELECT edit
					FROM DRIBBLE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$drirayon2=current($db->query('SELECT rayon
					FROM DRIBBLE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$dridbase2=current($db->query('SELECT dbase
					FROM DRIBBLE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());

				$driX3=current($db->query('SELECT X
					FROM DRIBBLE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$driY3=current($db->query('SELECT Y
					FROM DRIBBLE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$driedit3=current($db->query('SELECT edit
					FROM DRIBBLE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$drirayon3=current($db->query('SELECT rayon
					FROM DRIBBLE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$dridbase3=current($db->query('SELECT dbase
					FROM DRIBBLE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());





				$nbrdri2 =current($db->query('SELECT COUNT(*) FROM DRIBLE2PTS WHERE  appartient = "'.$appartient.'"')->fetch());
				$tailledri2=intval($nbrdri2);
				$dri2X0=current($db->query('SELECT X
					FROM DRIBLE2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dri2Y0=current($db->query('SELECT Y
					FROM DRIBLE2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dri2edit0=current($db->query('SELECT edit
					FROM DRIBLE2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dri2rayon0=current($db->query('SELECT rayon
					FROM DRIBLE2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$dri2dbase0=current($db->query('SELECT dbase
					FROM DRIBLE2PTS
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$dri2X1=current($db->query('SELECT X
					FROM DRIBLE2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dri2Y1=current($db->query('SELECT Y
					FROM DRIBLE2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dri2edit1=current($db->query('SELECT edit
					FROM DRIBLE2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dri2rayon1=current($db->query('SELECT rayon
					FROM DRIBLE2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$dri2dbase1=current($db->query('SELECT dbase
					FROM DRIBLE2PTS
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());





				$nbrpassse=current($db->query('SELECT COUNT(*) FROM PASSES WHERE  appartient = "'.$appartient.'"')->fetch());
				$taillepasse=intval($nbrpassse);
				$passedep0=current($db->query('SELECT numdep
					FROM PASSES
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$passearr0=current($db->query('SELECT numarr
					FROM PASSES
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$passedep1=current($db->query('SELECT numdep
					FROM PASSES
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$passearr1=current($db->query('SELECT numarr
					FROM PASSES
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());





				$nbrlast=current($db->query('SELECT COUNT(*) FROM LAST WHERE appartient = "'.$appartient.'"')->fetch());
				$taillelast=intval($nbrlast);
				$lastact0=current($db->query('SELECT action
					FROM LAST
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$lastarr0=current($db->query('SELECT arrive
					FROM LAST
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$lastdep0=current($db->query('SELECT depart
					FROM LAST
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$lastact1=current($db->query('SELECT action
					FROM LAST
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$lastarr1=current($db->query('SELECT arrive
					FROM LAST
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$lastdep1=current($db->query('SELECT depart
					FROM LAST
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());






				$nbrzone=current($db->query('SELECT COUNT(*) FROM ZONE WHERE appartient = "'.$appartient.'"')->fetch());
				$taillezone=intval($nbrzone);
				$zoneX0=current($db->query('SELECT X
					FROM ZONE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$zoneY0=current($db->query('SELECT Y
					FROM ZONE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$zonerayon0=current($db->query('SELECT rayon
					FROM ZONE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$zoneisDragging0=current($db->query('SELECT isDragging
					FROM ZONE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());
				$zoneedit0=current($db->query('SELECT edit
					FROM ZONE
					WHERE numero = 1 and appartient = "'.$appartient.'"')->fetch());

				$zoneX1=current($db->query('SELECT X
					FROM ZONE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$zoneY1=current($db->query('SELECT Y
					FROM ZONE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$zonerayon1=current($db->query('SELECT rayon
					FROM ZONE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$zoneisDragging1=current($db->query('SELECT isDragging
					FROM ZONE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());
				$zoneedit1=current($db->query('SELECT edit
					FROM ZONE
					WHERE numero = 2 and appartient = "'.$appartient.'"')->fetch());

				$zoneX2=current($db->query('SELECT X
					FROM ZONE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$zoneY2=current($db->query('SELECT Y
					FROM ZONE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$zonerayon2=current($db->query('SELECT rayon
					FROM ZONE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$zoneisDragging2=current($db->query('SELECT isDragging
					FROM ZONE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());
				$zoneedit2=current($db->query('SELECT edit
					FROM ZONE
					WHERE numero = 3 and appartient = "'.$appartient.'"')->fetch());

				$zoneX3=current($db->query('SELECT X
					FROM ZONE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$zoneY3=current($db->query('SELECT Y
					FROM ZONE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$zonerayon3=current($db->query('SELECT rayon
					FROM ZONE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$zoneisDragging3=current($db->query('SELECT isDragging
					FROM ZONE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());
				$zoneedit3=current($db->query('SELECT edit
					FROM ZONE
					WHERE numero = 4 and appartient = "'.$appartient.'"')->fetch());

				$zoneX4=current($db->query('SELECT X
					FROM ZONE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$zoneY4=current($db->query('SELECT Y
					FROM ZONE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$zonerayon4=current($db->query('SELECT rayon
					FROM ZONE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$zoneisDragging4=current($db->query('SELECT isDragging
					FROM ZONE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());
				$zoneedit4=current($db->query('SELECT edit
					FROM ZONE
					WHERE numero = 5 and appartient = "'.$appartient.'"')->fetch());

				$typeter=current($db->query('SELECT type
					FROM TYPETERRAIN
					WHERE appartient = "'.$appartient.'"')->fetch());

					?>

					<input type="hidden" id="taille" name="taille" value= <?php echo $taille ?>>
					<input type="hidden" id="numer0" name="numer0" value= <?php echo $numero0 ?>>
					<input type="hidden" id="X0" name="posX0" value= <?php echo $posX0 ?>>
					<input type="hidden" id="Y0" name="Y0" value= <?php echo $posY0 ?>>
					<input type="hidden" id="rayo0" name="rayo0" value= <?php echo $rayon0 ?>>
					<input type="hidden" id="couleu0" name="couleu0" value= <?php echo $couleur0 ?>>
					<input type="hidden" id="gotthebal0" name="gotthebal0" value= <?php echo $gottheball0 ?>>
					<input type="hidden" id="setbal0" name="setbal0" value= <?php echo $setball0 ?>>
					<input type="hidden" id="sedeplac0" name="sedeplac0" value= <?php echo $sedeplace0 ?>>
					<input type="hidden" id="tirin0" name="tirin0" value= <?php echo $tiring0 ?>>
					<input type="hidden" id="isDraggin0" name="isDraggin0" value= <?php echo $isDragging0 ?>>
					<input type="hidden" id="dribb0" name="dribb0" value= <?php echo $dribb0 ?>>


					<input type="hidden" id="numer1" name="numer1" value= <?php echo $numero1 ?>>
					<input type="hidden" id="X1" name="X1" value= <?php echo $posX1 ?>>
					<input type="hidden" id="Y1" name="Y1" value= <?php echo $posY1 ?>>
					<input type="hidden" id="rayo1" name="rayo1" value= <?php echo $rayon1 ?>>
					<input type="hidden" id="couleu1" name="couleu1" value= <?php echo $couleur1 ?>>
					<input type="hidden" id="gotthebal1" name="gotthebal1" value= <?php echo $gottheball1 ?>>
					<input type="hidden" id="setbal1" name="setbal1" value= <?php echo $setball1 ?>>
					<input type="hidden" id="sedeplac1" name="sedeplac1" value= <?php echo $sedeplace1 ?>>
					<input type="hidden" id="tirin1" name="tirin1" value= <?php echo $tiring1 ?>>
					<input type="hidden" id="isDraggin1" name="isDraggin1" value= <?php echo $isDragging1 ?>>
					<input type="hidden" id="dribb1" name="dribb1" value= <?php echo $dribb1 ?>>


					<input type="hidden" id="numer2" name="numer2" value= <?php echo $numero2 ?>>
					<input type="hidden" id="X2" name="X2" value= <?php echo $posX2 ?>>
					<input type="hidden" id="Y2" name="Y2" value= <?php echo $posY2 ?>>
					<input type="hidden" id="rayo2" name="rayo2" value= <?php echo $rayon2 ?>>
					<input type="hidden" id="couleu2" name="couleu2" value= <?php echo $couleur2 ?>>
					<input type="hidden" id="gotthebal2" name="gotthebal2" value= <?php echo $gottheball2 ?>>
					<input type="hidden" id="setbal2" name="setbal2" value= <?php echo $setball2 ?>>
					<input type="hidden" id="sedeplac2" name="sedeplac2" value= <?php echo $sedeplace2 ?>>
					<input type="hidden" id="tirin2" name="tirin2" value= <?php echo $tiring2 ?>>
					<input type="hidden" id="isDraggin2" name="isDraggin2" value= <?php echo $isDragging2 ?>>
					<input type="hidden" id="dribb2" name="dribb2" value= <?php echo $dribb2 ?>>


					<input type="hidden" id="numer3" name="numer3" value= <?php echo $numero3 ?>>
					<input type="hidden" id="X3" name="X3" value= <?php echo $posX3 ?>>
					<input type="hidden" id="Y3" name="Y3" value= <?php echo $posY3 ?>>
					<input type="hidden" id="rayo3" name="rayo3" value= <?php echo $rayon3 ?>>
					<input type="hidden" id="couleu3" name="couleu3" value= <?php echo $couleur3 ?>>
					<input type="hidden" id="gotthebal3" name="gotthebal3" value= <?php echo $gottheball3 ?>>
					<input type="hidden" id="setbal3" name="setbal3" value= <?php echo $setball3 ?>>
					<input type="hidden" id="sedeplac3" name="sedeplac3" value= <?php echo $sedeplace3 ?>>
					<input type="hidden" id="tirin3" name="tirin3" value= <?php echo $tiring3 ?>>
					<input type="hidden" id="isDraggin3" name="isDraggin3" value= <?php echo $isDragging3 ?>>
					<input type="hidden" id="dribb3" name="dribb3" value= <?php echo $dribb3 ?>>


					<input type="hidden" id="numer4" name="numer4" value= <?php echo $numero4 ?>>
					<input type="hidden" id="X4" name="X4" value= <?php echo $posX4 ?>>
					<input type="hidden" id="Y4" name="Y4" value= <?php echo $posY4 ?>>
					<input type="hidden" id="rayo4" name="rayo4" value= <?php echo $rayon4 ?>>
					<input type="hidden" id="couleu4" name="couleu4" value= <?php echo $couleur4 ?>>
					<input type="hidden" id="gotthebal4" name="gotthebal4" value= <?php echo $gottheball4 ?>>
					<input type="hidden" id="setbal4" name="setbal4" value= <?php echo $setball4 ?>>
					<input type="hidden" id="sedeplac4" name="sedeplac4" value= <?php echo $sedeplace4 ?>>
					<input type="hidden" id="tirin4" name="tirin4" value= <?php echo $tiring4 ?>>
					<input type="hidden" id="isDraggin4" name="isDraggin4" value= <?php echo $isDragging4 ?>>
					<input type="hidden" id="dribb4" name="dribb4" value= <?php echo $dribb4 ?>>












					<input type="hidden" id="tailleAd" name="tailleAd" value= <?php echo $tailleAd ?>>

					<input type="hidden" id="defX0" name="defX0" value= <?php echo $defX0 ?>>
					<input type="hidden" id="defY0" name="defY0" value= <?php echo $defY0 ?>>
					<input type="hidden" id="defrayon0" name="defrayon0" value= <?php echo $defrayon0 ?>>
					<input type="hidden" id="defcouleur0" name="defcouleur0" value= <?php echo $defcouleur0 ?>>
					<input type="hidden" id="defisDragging0" name="defisDragging0" value= <?php echo $defisDragging0 ?>>
					<input type="hidden" id="defsetball0" name="defsetball0" value= <?php echo $defisDragging0 ?>>
					<input type="hidden" id="defhig0" name="defhig0" value= <?php echo $defhig0 ?>>
					<input type="hidden" id="defangle0" name="defangle0" value= <?php echo $defangle0 ?>>
					<input type="hidden" id="definverse0" name="definverse0" value= <?php echo $definverse0 ?>>
					<input type="hidden" id="defrretour0" name="defrretour0" value= <?php echo $defrretour0 ?>>
					<input type="hidden" id="defrangle0" name="defrangle0" value= <?php echo $defrangle0 ?>>


					<input type="hidden" id="defX1" name="defX1" value= <?php echo $defX1 ?>>
					<input type="hidden" id="defY1" name="defY1" value= <?php echo $defY1 ?>>
					<input type="hidden" id="defrayon1" name="defrayon1" value= <?php echo $defrayon1 ?>>
					<input type="hidden" id="defcouleur1" name="defcouleur1" value= <?php echo $defcouleur1 ?>>
					<input type="hidden" id="defsetball1" name="defisDragging1" value= <?php echo $defisDragging1 ?>>
					<input type="hidden" id="defisDragging1" name="defsetball1" value= <?php echo $defisDragging01?>>
					<input type="hidden" id="defhig1" name="defhig1" value= <?php echo $defhig1 ?>>
					<input type="hidden" id="defangle1" name="defangle1" value= <?php echo $defangle1 ?>>
					<input type="hidden" id="definverse1" name="definverse1" value= <?php echo $definverse1?>>
					<input type="hidden" id="defrretour1" name="defrretour1" value= <?php echo $defrretour1?>>
					<input type="hidden" id="defrangle1" name="defrangle1" value= <?php echo $defrangle1 ?>>


					<input type="hidden" id="defX2" name="defX2" value= <?php echo $defX2 ?>>
					<input type="hidden" id="defY2" name="defY2" value= <?php echo $defY2 ?>>
					<input type="hidden" id="defrayon2" name="defrayon2" value= <?php echo $defrayon2 ?>>
					<input type="hidden" id="defcouleur2" name="defcouleur2" value= <?php echo $defcouleur2 ?>>
					<input type="hidden" id="defisDragging2" name="defisDragging2" value= <?php echo $defisDragging2 ?>>
					<input type="hidden" id="defsetball2" name="defsetball2" value= <?php echo $defisDragging2 ?>>
					<input type="hidden" id="defhig2" name="defhig2" value= <?php echo $defhig2 ?>>
					<input type="hidden" id="defangle2" name="defangle2" value= <?php echo $defangle2 ?>>
					<input type="hidden" id="definverse2" name="definverse2" value= <?php echo $definverse2 ?>>
					<input type="hidden" id="defrretour2" name="defrretour2" value= <?php echo $defrretour2 ?>>
					<input type="hidden" id="defrangle2" name="defrangle2" value= <?php echo $defrangle2 ?>>


					<input type="hidden" id="defX3" name="defX3" value= <?php echo $defX3 ?>>
					<input type="hidden" id="defY3" name="defY3" value= <?php echo $defY3 ?>>
					<input type="hidden" id="defrayon3" name="defrayon0" value= <?php echo $defrayon3 ?>>
					<input type="hidden" id="defcouleur3" name="defcouleur3" value= <?php echo $defcouleur3 ?>>
					<input type="hidden" id="defisDragging3" name="defisDragging3" value= <?php echo $defisDragging3 ?>>
					<input type="hidden" id="defsetball3" name="defsetball3" value= <?php echo $defisDragging3 ?>>
					<input type="hidden" id="defhig3" name="defhig3" value= <?php echo $defhig3 ?>>
					<input type="hidden" id="defangle3" name="defangle3" value= <?php echo $defangle3 ?>>
					<input type="hidden" id="definverse3" name="definverse3" value= <?php echo $definverse3 ?>>
					<input type="hidden" id="defrretour3" name="defrretour3" value= <?php echo $defrretour3 ?>>
					<input type="hidden" id="defrangle3" name="defrangle3" value= <?php echo $defrangle3 ?>>



					<input type="hidden" id="defX4" name="defX4" value= <?php echo $defX4 ?>>
					<input type="hidden" id="defY4" name="defY4" value= <?php echo $defY4 ?>>
					<input type="hidden" id="defrayon4" name="defrayon4" value= <?php echo $defrayon4 ?>>
					<input type="hidden" id="defcouleur4" name="defcouleur4" value= <?php echo $defcouleur4 ?>>
					<input type="hidden" id="defisDragging4" name="defisDragging4" value= <?php echo $defisDragging4 ?>>
					<input type="hidden" id="defsetball4" name="defsetball4" value= <?php echo $defisDragging4 ?>>
					<input type="hidden" id="defhig4" name="defhig4" value= <?php echo $defhig4 ?>>
					<input type="hidden" id="defangle4" name="defangle4" value= <?php echo $defangle4 ?>>
					<input type="hidden" id="definverse4" name="definverse4" value= <?php echo $definverse4 ?>>
					<input type="hidden" id="defrretour4" name="defrretour4" value= <?php echo $defrretour4 ?>>
					<input type="hidden" id="defrangle4" name="defrangle4" value= <?php echo $defrangle4 ?>>









					<input type="hidden" id="tailledep" name="tailledep" value= <?php echo $tailledep ?>>

					<input type="hidden" id="depX0" name="depX0" value= <?php echo $depX0 ?>>
					<input type="hidden" id="depY0" name="depY0" value= <?php echo $depY0 ?>>
					<input type="hidden" id="depedit0" name="depedit0" value= <?php echo $depedit0 ?>>
					<input type="hidden" id="deprayon0" name="deprayon0" value= <?php echo $deprayon0 ?>>
					<input type="hidden" id="depdbase0" name="depdbase0" value= <?php echo $depdbase0 ?>>
					<input type="hidden" id="depec0" name="depec0" value= <?php echo $depec0 ?>>
					<input type="hidden" id="dephig0" name="dephig0" value= <?php echo $dephig0 ?>>
					<input type="hidden" id="depangle0" name="depangle0" value= <?php echo $depangle0 ?>>
					<input type="hidden" id="depinverse0" name="depinverse0" value= <?php echo $depinverse0 ?>>

					<input type="hidden" id="depX1" name="depX1" value= <?php echo $depX1 ?>>
					<input type="hidden" id="depY1" name="depY1" value= <?php echo $depY1 ?>>
					<input type="hidden" id="depedit1" name="depedit1" value= <?php echo $depedit1 ?>>
					<input type="hidden" id="deprayon1" name="deprayon1" value= <?php echo $deprayon1 ?>>
					<input type="hidden" id="depdbase1" name="depdbase1" value= <?php echo $depdbase1?>>
					<input type="hidden" id="depec1" name="depec1" value= <?php echo $depec1 ?>>
					<input type="hidden" id="dephig1" name="dephig1" value= <?php echo $dephig1 ?>>
					<input type="hidden" id="depangle1" name="depangle1" value= <?php echo $depangle1 ?>>
					<input type="hidden" id="depinverse1" name="depinverse1" value= <?php echo $depinverse1 ?>>

					<input type="hidden" id="depX2" name="depX2" value= <?php echo $depX2 ?>>
					<input type="hidden" id="depY2" name="depY2" value= <?php echo $depY2 ?>>
					<input type="hidden" id="depedit2" name="depedit2" value= <?php echo $depedit2 ?>>
					<input type="hidden" id="deprayon2" name="deprayon2" value= <?php echo $deprayon2 ?>>
					<input type="hidden" id="depdbase2" name="depdbase2" value= <?php echo $depdbase2 ?>>
					<input type="hidden" id="depec2" name="depec2" value= <?php echo $depec2 ?>>
					<input type="hidden" id="dephig2" name="dephig2" value= <?php echo $dephig2 ?>>
					<input type="hidden" id="depangle2" name="depangle2" value= <?php echo $depangle2 ?>>
					<input type="hidden" id="depinverse2" name="depinverse2" value= <?php echo $depinverse2 ?>>

					<input type="hidden" id="depX3" name="depX3" value= <?php echo $depX3 ?>>
					<input type="hidden" id="depY3" name="depY3" value= <?php echo $depY3 ?>>
					<input type="hidden" id="depedit3" name="depedit3" value= <?php echo $depedit3 ?>>
					<input type="hidden" id="deprayon3" name="deprayon3" value= <?php echo $deprayon3 ?>>
					<input type="hidden" id="depdbase3" name="depdbase3" value= <?php echo $depdbase3 ?>>
					<input type="hidden" id="depec3" name="depec3" value= <?php echo $depec3 ?>>
					<input type="hidden" id="dephig3" name="dephig3" value= <?php echo $dephig3 ?>>
					<input type="hidden" id="depangle3" name="depangle3" value= <?php echo $depangle3 ?>>
					<input type="hidden" id="depinverse3" name="depinverse3" value= <?php echo $depinverse3 ?>>










					<input type="hidden" id="tailledep2" name="tailledep2" value= <?php echo $tailledep2 ?>>

					<input type="hidden" id="dep2X0" name="dep2X0" value= <?php echo $dep2X0 ?>>
					<input type="hidden" id="dep2Y0" name="dep2Y0" value= <?php echo $dep2Y0 ?>>
					<input type="hidden" id="dep2edit0" name="dep2edit0" value= <?php echo $dep2edit0 ?>>
					<input type="hidden" id="dep2rayon0" name="dep2rayon0" value= <?php echo $dep2rayon0 ?>>
					<input type="hidden" id="dep2dbase0" name="dep2dbase0" value= <?php echo $dep2dbase0 ?>>
					<input type="hidden" id="dep2ec0" name="dep2ec0" value= <?php echo $dep2ec0 ?>>
					<input type="hidden" id="dep2hig0" name="dep2hig0" value= <?php echo $dep2hig0 ?>>
					<input type="hidden" id="dep2angle0" name="dep2angle0" value= <?php echo $dep2angle0 ?>>
					<input type="hidden" id="dep2inverse0" name="dep2inverse0" value= <?php echo $dep2inverse0 ?>>

					<input type="hidden" id="dep2X1" name="dep2X1" value= <?php echo $dep2X1 ?>>
					<input type="hidden" id="dep2Y1" name="dep2Y1" value= <?php echo $dep2Y1 ?>>
					<input type="hidden" id="dep2edit1" name="dep2edit1" value= <?php echo $dep2edit1 ?>>
					<input type="hidden" id="dep2rayon1" name="dep2rayon1" value= <?php echo $dep2rayon1 ?>>
					<input type="hidden" id="dep2dbase1" name="dep2dbase1" value= <?php echo $dep2dbase1?>>
					<input type="hidden" id="dep2ec1" name="dep2ec1" value= <?php echo $dep2ec1 ?>>
					<input type="hidden" id="dep2hig1" name="dep2hig1" value= <?php echo $dep2hig1 ?>>
					<input type="hidden" id="dep2angle1" name="dep2angle1" value= <?php echo $dep2angle1 ?>>
					<input type="hidden" id="dep2inverse1" name="dep2inverse1" value= <?php echo $dep2inverse1 ?>>

					<input type="hidden" id="dep2X2" name="dep2X2" value= <?php echo $dep2X2 ?>>
					<input type="hidden" id="dep2Y2" name="dep2Y2" value= <?php echo $dep2Y2 ?>>
					<input type="hidden" id="dep2edit2" name="dep2edit2" value= <?php echo $dep2edit2 ?>>
					<input type="hidden" id="dep2rayon2" name="dep2rayon2" value= <?php echo $dep2rayon2 ?>>
					<input type="hidden" id="dep2dbase2" name="dep2dbase2" value= <?php echo $dep2dbase2 ?>>
					<input type="hidden" id="dep2ec2" name="dep2ec2" value= <?php echo $dep2ec2 ?>>
					<input type="hidden" id="dep2hig2" name="dep2hig2" value= <?php echo $dep2hig2 ?>>
					<input type="hidden" id="dep2angle2" name="dep2angle2" value= <?php echo $dep2angle2 ?>>
					<input type="hidden" id="dep2inverse2" name="dep2inverse2" value= <?php echo $dep2inverse2 ?>>

					<input type="hidden" id="dep2X3" name="dep2X3" value= <?php echo $dep2X3 ?>>
					<input type="hidden" id="dep2Y3" name="dep2Y3" value= <?php echo $dep2Y3 ?>>
					<input type="hidden" id="dep2edit3" name="dep2edit3" value= <?php echo $dep2edit3 ?>>
					<input type="hidden" id="dep2rayon3" name="dep2rayon3" value= <?php echo $dep2rayon3 ?>>
					<input type="hidden" id="dep2dbase3" name="dep2dbase3" value= <?php echo $dep2dbase3 ?>>
					<input type="hidden" id="dep2ec3" name="dep2ec3" value= <?php echo $dep2ec3 ?>>
					<input type="hidden" id="dep2hig3" name="dep2hig3" value= <?php echo $deph2ig3 ?>>
					<input type="hidden" id="dep2angle3" name="dep2angle3" value= <?php echo $dep2angle3 ?>>
					<input type="hidden" id="dep2inverse3" name="dep2inverse3" value= <?php echo $dep2inverse3 ?>>









					<input type="hidden" id="tailledri" name="tailledri" value= <?php echo $tailledri ?>>

					<input type="hidden" id="driX0" name="driX0" value= <?php echo $driX0 ?>>
					<input type="hidden" id="driY0" name="driY0" value= <?php echo $driY0 ?>>
					<input type="hidden" id="driedit0" name="driedit0" value= <?php echo $driedit0 ?>>
					<input type="hidden" id="drirayon0" name="drirayon0" value= <?php echo $drirayon0 ?>>
					<input type="hidden" id="dridbase0" name="dridbase0" value= <?php echo $dridbase0 ?>>

					<input type="hidden" id="driX1" name="driX1" value= <?php echo $driX1 ?>>
					<input type="hidden" id="driY1" name="driY1" value= <?php echo $driY1 ?>>
					<input type="hidden" id="driedit1" name="driedit1" value= <?php echo $driedit1 ?>>
					<input type="hidden" id="drirayon1" name="drirayon1" value= <?php echo $drirayon1 ?>>
					<input type="hidden" id="dridbase1" name="dridbase1" value= <?php echo $dridbase1 ?>>

					<input type="hidden" id="driX2" name="driX2" value= <?php echo $driX2 ?>>
					<input type="hidden" id="driY2" name="driY2" value= <?php echo $driY2 ?>>
					<input type="hidden" id="driedit2" name="driedit2" value= <?php echo $driedit2 ?>>
					<input type="hidden" id="drirayon2" name="drirayon2" value= <?php echo $drirayon2 ?>>
					<input type="hidden" id="dridbase2" name="dridbase2" value= <?php echo $dridbase2 ?>>

					<input type="hidden" id="driX3" name="driX3" value= <?php echo $driX3 ?>>
					<input type="hidden" id="driY3" name="driY3" value= <?php echo $driY3 ?>>
					<input type="hidden" id="driedit3" name="driedit3" value= <?php echo $driedit3 ?>>
					<input type="hidden" id="drirayon3" name="drirayon3" value= <?php echo $drirayon3 ?>>
					<input type="hidden" id="dridbase3" name="dridbase3" value= <?php echo $dridbase3 ?>>










					<input type="hidden" id="tailledri2" name="tailledri2" value= <?php echo $tailledri2 ?>>

					<input type="hidden" id="dri2X0" name="dri2X0" value= <?php echo $dri2X0 ?>>
					<input type="hidden" id="dri2Y0" name="dri2Y0" value= <?php echo $dri2Y0 ?>>
					<input type="hidden" id="dri2edit0" name="dri2edit0" value= <?php echo $dri2edit0 ?>>
					<input type="hidden" id="dri2rayon0" name="dri2rayon0" value= <?php echo $dri2rayon0 ?>>
					<input type="hidden" id="dri2dbase0" name="dri2dbase0" value= <?php echo $dri2dbase0 ?>>

					<input type="hidden" id="dri2X1" name="dri2X1" value= <?php echo $dri2X1 ?>>
					<input type="hidden" id="dri2Y1" name="dri2Y1" value= <?php echo $dri2Y1 ?>>
					<input type="hidden" id="dri2edit1" name="dri2edit1" value= <?php echo $dri2edit1 ?>>
					<input type="hidden" id="dri2rayon1" name="dri2rayon1" value= <?php echo $dri2rayon1 ?>>
					<input type="hidden" id="dri2dbase1" name="dri2dbase1" value= <?php echo $dri2dbase1 ?>>








					<input type="hidden" id="taillepasse" name="taillepasse" value= <?php echo $taillepasse ?>>

					<input type="hidden" id="passedep0" name="passedep0" value= <?php echo $passedep0 ?>>
					<input type="hidden" id="passearr0" name="passearr0" value= <?php echo $passearr0 ?>>

					<input type="hidden" id="passedep1" name="passedep1" value= <?php echo $passedep1 ?>>
					<input type="hidden" id="passearr1" name="passearr1" value= <?php echo $passearr1 ?>>







					<input type="hidden" id="taillelast" name="taillelast" value= <?php echo $taillelast ?>>

					<input type="hidden" id="lastact0" name="lastact0" value= <?php echo $lastact0 ?>>
					<input type="hidden" id="lastarr0" name="lastarr0" value= <?php echo $lastarr0 ?>>
					<input type="hidden" id="lastdep0" name="lastdep0" value= <?php echo $lastdep0 ?>>

					<input type="hidden" id="lastact1" name="lastact1" value= <?php echo $lastact1 ?>>
					<input type="hidden" id="lastarr1" name="lastarr1" value= <?php echo $lastarr1 ?>>
					<input type="hidden" id="lastdep1" name="lastdep1" value= <?php echo $lastdep1 ?>>











					<input type="hidden" id="taillezone" name="taillezone" value= <?php echo $taillezone ?>>

					<input type="hidden" id="zoneX0" name="zoneX0" value= <?php echo $zoneX0 ?>>
					<input type="hidden" id="zoneY0" name="zoneY0" value= <?php echo $zoneY0 ?>>
					<input type="hidden" id="zonerayon0" name="zonerayon0" value= <?php echo $zonerayon0 ?>>
					<input type="hidden" id="zoneisDragging0" name="zoneisDragging0" value= <?php echo $zoneisDragging0 ?>>
					<input type="hidden" id="zoneedit0" name="zoneedit0" value= <?php echo $zoneedit0 ?>>

					<input type="hidden" id="zoneX1" name="zoneX1" value= <?php echo $zoneX1 ?>>
					<input type="hidden" id="zoneY1" name="zoneY1" value= <?php echo $zoneY1 ?>>
					<input type="hidden" id="zonerayon1" name="zonerayon1" value= <?php echo $zonerayon1 ?>>
					<input type="hidden" id="zoneisDragging1" name="zoneisDragging1" value= <?php echo $zoneisDragging1 ?>>
					<input type="hidden" id="zoneedit1" name="zoneedit1" value= <?php echo $zoneedit1 ?>>

					<input type="hidden" id="zoneX2" name="zoneX2" value= <?php echo $zoneX2 ?>>
					<input type="hidden" id="zoneY2" name="zoneY2" value= <?php echo $zoneY2 ?>>
					<input type="hidden" id="zonerayon2" name="zonerayon2" value= <?php echo $zonerayon2 ?>>
					<input type="hidden" id="zoneisDragging2" name="zoneisDragging2" value= <?php echo $zoneisDragging2 ?>>
					<input type="hidden" id="zoneedit2" name="zoneedit2" value= <?php echo $zoneedit2 ?>>

					<input type="hidden" id="zoneX3" name="zoneX3" value= <?php echo $zoneX3 ?>>
					<input type="hidden" id="zoneY3" name="zoneY3" value= <?php echo $zoneY3 ?>>
					<input type="hidden" id="zonerayon3" name="zonerayon3" value= <?php echo $zonerayon3 ?>>
					<input type="hidden" id="zoneisDragging3" name="zoneisDragging3" value= <?php echo $zoneisDragging3 ?>>
					<input type="hidden" id="zoneedit3" name="zoneedit3" value= <?php echo $zoneedit3 ?>>

					<input type="hidden" id="zoneX4" name="zoneX4" value= <?php echo $zoneX4 ?>>
					<input type="hidden" id="zoneY4" name="zoneY4" value= <?php echo $zoneY4 ?>>
					<input type="hidden" id="zonerayon4" name="zonerayon4" value= <?php echo $zonerayon4 ?>>
					<input type="hidden" id="zoneisDragging4" name="zoneisDragging4" value= <?php echo $zoneisDragging4 ?>>
					<input type="hidden" id="zoneedit4" name="zoneedit4" value= <?php echo $zoneedit4 ?>>
					<input type="hidden" id="typeter" name="typeter" value= <?php echo $typeter ?>>


					<input type="hidden" class="btn btn-warning fixed-bottom font-weight-bold mb-5" name="inputLoad" id="inputLoad" value="LOAD" onclick="charge()">
				</input>
			</form>
			<!-- Optional JavaScript -->
			<!-- jQuery first, then Popper.js, then Bootstrap JS -->
			<script src="rectest.js"></script>
			<script src="latex.js"></script>
			<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
			<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
			<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
			<?php
			$viensDeGalerie=$_GET['viensDeGalerie'];
			if($viensDeGalerie){
				?>
				<script type="text/javascript">
					window.onload=charge();
				</script>
				<?php
			}?>
		</body>
		</html>
